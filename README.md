[![pipeline status](https://gitlab.com/Durrendal/WS-Alpine-Packages/badges/master/pipeline.svg)](https://gitlab.com/Durrendal/WS-Alpine-Packages/commits/master)

# Personal APKBUILD Testing Repo

## Abandoned:
Half built APKBUILDs that either won't work because they depend on unsupported systems (like systemd), someone else has packaged it better, or I just lost interest.
These are left as reference if someone else wants to try.

## Testing:
The testing repo, anything I support gets worked on here before getting push to main, community, or testing at Alpine

## Non-Free:
Anything with a non-free license. These don't have any repo with Alpine, so while I can make an MR for them, it will be accepted, nobody will be able to use them. I just keep these for personal use, and should someone else want them. They don't get updated as frequently.