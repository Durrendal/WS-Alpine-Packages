# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=py3-vt-py
_pkg=vt-py
pkgver=0.17.5
pkgrel=0
pkgdesc="Official Python client library for VirusTotal's REST API"
url="https://github.com/virustotal/${_base}"
license="Apache"
arch="noarch"
makedepends="py3-build py3-installer py3-setuptools py3-wheel py3-sphinx"
checkdepends="py3-pytest-asyncio py3-pytest-httpserver"
depends="python3 py3-aiohttp"
source="$pkgname-$pkgver.tar.gz::https://github.com/VirusTotal/vt-py/archive/refs/tags/$pkgver.tar.gz"
subpackages="$pkgname-pyc $pkgname-doc"
builddir="$srcdir/$_pkg-$pkgver"

prepare() {
	default_prepare
	#sed -i '/setup_requires/d' ${_base}-$pkgver/setup.py
}

build() {
	python3 -m build --wheel --skip-dependency-check --no-isolation
	sphinx-build -b man docs/source _build
}

check() {
	python3 -m venv --system-site-packages test-env
	test-env/bin/python -m installer dist/*.whl
	test-env/bin/python -m pytest
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
	install -Dm644 "_build/$_pkg.1" -t "$pkgdir/usr/share/man/man1/"
}

sha512sums="
4167ef4ba7e66f9b82e56a8aad0f5295213369233484278b508b70fe2fa06e01b797b2d28f2a9fe72040d073219d4ff741e2712112aafe239ae03491dde3dacf  py3-vt-py-0.17.5.tar.gz
"
