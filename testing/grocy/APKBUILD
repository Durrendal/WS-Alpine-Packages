# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=grocy
pkgver=3.1.3
pkgrel=0
pkgdesc="web-based self-hosted groceries & household management solution for your home"
url="https://grocy.info/"
license="MIT"
arch="noarch"
makedepends="composer yarn php8-dev php8-sqlite3 php8-gd php8-intl"
depends="php8 php8-fpm php8-sqlite3 php8-intl php8-gd php8-fileinfo php8-pdo_sqlite php8-ctype php8-mbstring php8-iconv php8-tokenizer sqlite"
source="$pkgname-$pkgver.zip::https://github.com/grocy/grocy/releases/download/v$pkgver/grocy_$pkgver.zip
	$pkgname-$pkgver.tar.gz::https://github.com/grocy/grocy/archive/refs/tags/v$pkgver.tar.gz
	grocy_nginx.conf
	grocy_nginx_fpm.conf"
options="!check" #no checks defined
subpackages="$pkgname-nginx:_nginx"

unpack() {
	#Web application is pre-packaged
	#Default unpack dumps this directly into the srcdir
	unzip $srcdir/$pkgname-$pkgver.zip -d $srcdir/$pkgname-$pkgver

	#But the web application lacks yarn & composer files to build with
	#so we pull and extract both
	mkdir $srcdir/source
	tar -xzvf $srcdir/$pkgname-$pkgver.tar.gz -C $srcdir/source
}

prepare(){
	#Pull yarn & composer files from tarball, and insert into webapp directory
	for f in yarn.lock composer.lock composer.json composer.lock package.json; do
		cp $srcdir/source/$pkgname-$pkgver/$f $srcdir/$pkgname-$pkgver/
	done
}

build() {
	php8 -n -dextension=gd -dextension=intl /usr/bin/composer install --no-interaction --no-dev --optimize-autoloader
	php8 /usr/bin/composer clear-cache

	yarn install --modules-folder public/node_modules --production
	yarn cache clean
}

package() {
	_instdir="$pkgdir"/usr/share/webapps/grocy
	mkdir -p "$_instdir" "$pkgdir"/etc/webapps/grocy "$pkgdir"/var/lib/webapps
	
	cp -r "$builddir"/* "$_instdir"
	mv "$pkgdir"/usr/share/webapps/grocy/data "$pkgdir"/var/lib/webapps/grocy

	ln -s /var/lib/webapps/grocy "$pkgdir"/usr/share/webapps/grocy/data
	ln -s /etc/webapps/grocy/config.php "$pkgdir"/var/lib/webapps/grocy/config.php

	mv "$builddir"/config-dist.php "$pkgdir"/etc/webapps/grocy/config.php

	#nginx:www-data
	chown -R 100:82 "$pkgdir"/usr/share/webapps/grocy
	chown -R 100:82 "$pkgdir"/var/lib/webapps/grocy

	#Set directory perms
	find "$pkgdir"/var/lib/webapps -type d -exec chmod 0775 {} \;
}

_nginx() {
	pkgdesc="Nginx configuration for Grocy"
	depends="nginx !grocy-lighttpd"
	install="$subpkgname".post-install

	install -d "$subpkgdir"/etc/nginx/sites-available
	install -d "$subpkgdir"/etc/php8/conf-available
	install "$srcdir"/grocy_nginx.conf "$subpkgdir"/etc/nginx/sites-available
	install "$srcdir"/grocy_nginx_fpm.conf "$subpkgdir"/etc/php8/conf-available
}

sha512sums="
cd77b1928de65a6c8d6b3268fde6dd41091ff687bba92819a49ef68cc6ef5f1739d7b35f274feb60f1ddd818c7198bb215ef24cabf4e7f4ffa8e61cf9c4951ba  grocy-3.1.3.zip
e90723f408db3d34d2bddb41b7644ddd79672d929dc60a7dd5bef638adddec46fb55b535eda1a32fff0adfe09dda9628e65464db0159cd93bbf0a4cbda0527a3  grocy-3.1.3.tar.gz
20f698a7b634ef6390f31275f2c0f8ca645e626521cb252c5c248c33616bd744ec0270f62bd7ffb3b56220dc37829ec8cc2692789ea1efffad8ba098e4c5caae  grocy_nginx.conf
8a51fee21e2746d35c52b530a2741798f7a87fdce02eb954c80172158a33e7f21eca191551ef4c89bb50edeea41e31e260ccb2269ee6a385ef1a2990a479774e  grocy_nginx_fpm.conf
"