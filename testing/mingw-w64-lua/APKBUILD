# Contributor: "Will Sinatra <wpsinatra@gmail.com>"
# Maintainer: "Will Sinatra <wpsinatra@gmail.com>"
pkgname=mingw-w64-lua
pkgver=5.3.5
pkgrel=0
pkgdesc="A powerful light-weight programming language designed for extending applications. (mingw-w64)" 
url="http://www.lua.org/" 
license="MIT"
arch="all"
depends="mingw-w64-crt"
source="$url/ftp/lua-$pkgver.tar.gz"
options="!strip !buildflags staticlibs"

prepare () {  
  # build import lib
  pwd
  sed -i 's|$(AR) $@ $(BASE_O)|$(CC) -shared -Wl,--out-implib,liblua53.dll.a -o $@ $(BASE_O) $(LIBS)|g' Makefile
}

build () {
  for _arch in ${_architectures}; do
    rm -rf "$srcdir"/build-${_arch}
    cp -r "$srcdir"/lua-$pkgver "$srcdir"/build-${_arch} && pushd "$srcdir"/build-${_arch}
    make generic \
      ALL=lua53.dll \
      LUA_A="lua53.dll" \
      LUA_T="lua.exe" \
      CC=${_arch}-gcc \
      RANLIB="ls" \
      MYCFLAGS="-D_FORTIFY_SOURCE=2 -O2 -pipe -fno-plt -fexceptions --param=ssp-buffer-size=4" \
      SYSCFLAGS="-DLUA_BUILD_AS_DLL" \
      LIBS="-lssp"
  done
}

package () {
  for _arch in ${_architectures}; do
    
    make install INSTALL_TOP="$pkgdir"/usr/${_arch} TO_BIN="lua53.dll" TO_LIB="liblua53.dll.a"
    rm -r "$pkgdir"/usr/${_arch}/{share,man,lib/lua}
    ${_arch}-strip --strip-unneeded "$pkgdir"/usr/${_arch}/bin/*.dll
    ${_arch}-strip -g "$pkgdir"/usr/${_arch}/lib/*.a
  done
}

sha512sums="4f9516acc4659dfd0a9e911bfa00c0788f0ad9348e5724fe8fb17aac59e9c0060a64378f82be86f8534e49c6c013e7488ad17321bafcc787831d3d67406bd0f4  lua-5.3.5.tar.gz"
