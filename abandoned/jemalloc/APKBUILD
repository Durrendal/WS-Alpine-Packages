# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=jemalloc
pkgver=5.2.1
pkgrel=0
pkgdesc='General-purpose scalable concurrent malloc implementation - 4.x series'
url='http://www.canonware.com/jemalloc/'
license="BSD"
arch="noarch"
depends="musl"
source="https://github.com/jemalloc/jemalloc/releases/download/$pkgver/jemalloc-$pkgver.tar.bz2"
subpackages="$pkgname-static $pkgname-doc $pkgname-dev"
options="!check"

build() {
	./configure \
		--enable-autogen \
		--prefix=/usr
	make
}

package() {
	sed -i 's/5.2.1_0/5.2.1/g' $builddir/jemalloc.pc
	make DESTDIR="$pkgdir" install

	install -Dm644 COPYING "$pkgdir/usr/share/licenses/jemalloc/COPYING"
	chmod 644 "$pkgdir/usr/lib/libjemalloc_pic.a"
}

sha512sums="0bbb77564d767cef0c6fe1b97b705d368ddb360d55596945aea8c3ba5889fbce10479d85ad492c91d987caacdbbdccc706aa3688e321460069f00c05814fae02  jemalloc-5.2.1.tar.bz2"
